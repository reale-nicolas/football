<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\FootballServices;
use App\Exceptions\ConflictException;
use App\Exceptions\NotFoundException;

class LeagueController extends Controller
{
    protected $service;
    
    public function __construct(FootballServices $service)
    {
        $this->service  = $service;   
    }


    public function import(Request $request, $leagueCode) 
    {

        try {

            $this->service->importLeague(Str::upper($leagueCode));

            return response()->json(['message' => 'Successfully imported'], 201);

        } catch (ConflictException $e) {

            return response()->json(['message' => 'League already imported'], 409);

        } catch (NotFoundException $e ) {

            return response()->json(['message' => 'Not found'], 404);

        } catch (Exception $e) {
            return response()->json(['message' => 'Server error'], 504);
        }
    }


    public function count(Request $request, $leagueCode) 
    {

        try {

            $count = $this->service->totalPlayers(Str::upper($leagueCode));

            return response()->json(['total' => $count], 200);

        }  catch (NotFoundException $e ) {

            return response()->json(['message' => 'Not found'], 404);

        } catch (Exception $e) {
            return response()->json(['message' => 'Server error'], 504);
        }
    }
}
