<?php

namespace App\Providers;

use App\Api\Contracts\FootballClientInterface;
use App\Api\FootballClient;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(FootballClientInterface::class, function ($app) {

            $config = config('services.football');
            $client = new FootballClient($config);
            return $client;

        });
    }

    
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
