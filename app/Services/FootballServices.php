<?php

namespace App\Services;

use App\Models\TeamModel;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use App\Models\CompetitionModel;
use Illuminate\Support\Facades\DB;
use App\Exceptions\ConflictException;
use App\Exceptions\NotFoundException;
use App\Api\Contracts\FootballClientInterface;;

/**
 * To make request to the service and store results on d
 * atabase according to what is needed.
 */
class FootballServices  {

    protected $footballApi;


    
    public function __construct(FootballClientInterface $footballClient)
    {
        $this->footballApi = $footballClient;
    }


    /**
     * Gets league information from API and store it on the local database
     *
     * @param string $leagueCode
     * @return bool
     */
    public function importLeague (string $leagueCode) 
    {

        //Check if the league already exist on our database
        $league = CompetitionModel::where('code', $leagueCode)->first();


        if ($league) {

            throw new ConflictException();

        } else {

            //gets league and teams information from API as an array
            $apiLeagueResponse = json_decode(json_encode($this->footballApi->getLeagueInformation($leagueCode)), true);

            //gets league info
            $leagueInfo = $apiLeagueResponse['competition'];
            
            //gets teams info 
            $teamsInfo = $apiLeagueResponse['teams'];
            
            //check on the DB if there is any teams already existing.
            $teamsAlreadyExisting = array_column(
                TeamModel::select('shortName')
                    ->whereIn('shortName', array_column($teamsInfo, 'shortName'))
                    ->get()
                    ->toArray(), 
                
                'shortName'
            );

            
            //Filter those teams already inserted on the DB and re-index the array
            $teamsToInsert =  array_values(
                Arr::where($teamsInfo, function($team) use ($teamsAlreadyExisting) {
                    return !in_array($team['shortName'], $teamsAlreadyExisting);
                })
            );


            //get staff of those teams
            $teams = [];
            foreach ($teamsToInsert as $team) {

                $apiPlayersResponse = json_decode(json_encode($this->footballApi->getTeamInformation($team['id'])), true);

                //gets and save only PLAYERS
                $team['players']    = array_values(
                    Arr::where($apiPlayersResponse['squad'], function($item) {
                        return $item['role'] == 'PLAYER';
                    })
                );

                $teams[] = $team;
            }

            $leagueInfo['teams'] = $teams;

            $this->saveInfo($leagueInfo, $teamsAlreadyExisting);
            
            return true;

        }
        
    }



    /**
     * Return the number of player that belong to a specific league
     *
     * @param string $leagueCode
     * @return int
     */
    public function totalPlayers(string $leagueCode) {

        $league = CompetitionModel::where('code', $leagueCode)->firstOr( function() { 

            throw new NotFoundException();
        });

        $total = $league->playersCount();
        
        return $total['count'];
    }


    /**
     * Save or create the new relation between leagues and teams.
     * 
     * @param array $leagueInfo: array in a tree format contining the league, 
     * thier teams and player to insert on the database.
     * @param array $teamsToAssociate: array wwith those teams that already exist
     * and needs to be associated to the new league
     * 
     * @return void
     */
    protected function saveInfo(array $leagueInfo, array $teamsToAssociate = []) 
    {
        //Format data according to needs
        $leagueInfo['areaName'] = $leagueInfo['area']['name'];

        try {
            DB::beginTransaction();

            //create the league
            $leagueModel = CompetitionModel::create($leagueInfo);

            foreach ($leagueInfo['teams'] as $team) {
                $team['areaName'] = $team['area']['name'];

                //sve the team
                $teamModel = $leagueModel->teams()->create($team);

                //reformat 'dateOfBirth'
                $team['players'] = array_map(function($element) {
                    $dt = new Carbon($element['dateOfBirth']);
                    $element['dateOfBirth'] = $dt->toDateString();

                    return $element;
                }, $team['players']);
                
                //save all the players realted to the current team
                $teamModel->players()->createMany($team['players']);
            }

            //Create the realtion between the new league and the existing teams.
            if (count($teamsToAssociate) > 0) {
                $teamsIdsToAssociate = TeamModel::select('id as team_id')->whereIn('shortName', $teamsToAssociate)->get()->toArray();
                $leagueModel->teams()->attach($teamsIdsToAssociate);
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }
}