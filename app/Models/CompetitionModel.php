<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CompetitionModel extends Model
{
    use HasFactory;

    protected $table = 'competition';

    protected $fillable = [
        'name',
        'code',
        'areaName',
    ];


    /**
     * Gets the league's teams.
     */
    public function teams()
    {
        return $this->belongsToMany(TeamModel::class, 'competition_team', 'competition_id', 'team_id');
    }


    /**
     * Get all of the deployments for the project.
     */
    public function players()
    {
        return $this->hasManyThrough(PlayerModel::class, TeamModel::class);
    }

    /**
     * Get all of the deployments for the project.
     */
    public function playersCount()
    {
        return $this
            ->select(DB::raw('count("player.id") as count'))
            ->join('competition_team', 'competition.id', '=', 'competition_team.competition_id')
            ->join('team', 'team.id', '=', 'competition_team.team_id')
            ->join('player', 'player.team_id', '=', 'competition_team.team_id')
            ->where('competition.id', $this->id)
            ->groupBy('competition_team.competition_id')
            ->first()
            ->toArray();
    }

  
}
