<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlayerModel extends Model
{
    use HasFactory;

    protected $table = 'player';

    protected $fillable = [
        'name',
        'position',
        'dateOfBirth',
        'countryOfBirth',
        'nationality',
    ];


    /**
     * Get the team player.
     */
    public function teams()
    {
        return $this->belongsTo(TeamModel::class, 'team_id');
    }
}
