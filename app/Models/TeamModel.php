<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeamModel extends Model
{
    use HasFactory;

    protected $table = 'team';

    protected $fillable = [
        'name',
        'tla',
        'shortName',
        'areaName',
        'email',
    ];



    /**
     * Get the league's team.
     */
    public function leagues()
    {
        return $this->belongsToMany(CompetitionModel::class, 'competition_team', 'team_id', 'competition_id');
    }


    /**
     * Gets the team's players.
     */
    public function players()
    {
        return $this->hasMany(PlayerModel::class, 'team_id');
        //return $this->belongsToMany(PlayerModel::class, 'team_pleayer', 'team_id', 'player_id');
    }

}
