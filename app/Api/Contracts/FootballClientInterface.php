<?php

namespace App\Api\Contracts;


interface FootballClientInterface {

    public function getLeagueInformation( string $leagueCode);

    public function getTeamInformation( string $teamId);

}