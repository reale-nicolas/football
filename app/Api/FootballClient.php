<?php

namespace App\Api;

use Exception;
use Illuminate\Support\Str;

use GuzzleHttp\Client as HttpClient;
use App\Exceptions\NotFoundException;
use App\Exceptions\ServiceUnavailable;
use GuzzleHttp\Exception\ClientException;
use App\Api\Contracts\FootballClientInterface;

/**
 * Class client to reach football-data API
 */
class FootballClient implements FootballClientInterface {

    const SECONDS_BETWEEN_REQUEST       = 70;
    private static $TEAMS_ENDPOINT      = '/v2/competitions/{{leagueId}}/teams';
    private static $PLAYERS_ENDPOINT    = '/v2/teams/{{teamId}}';

    protected $http;
    
	protected $baseURL;
	protected $accessToken;


	public function __construct ( array $config ) {

		$this->baseURL      = $config['base_url'];
		$this->accessToken  = $config['token'] ;

		$this->http = new HttpClient([
            // Base URI is used with relative requests
            'base_uri' => $this->baseURL
        ]);

	}


    /**
     * Gets league information by code and teams that belongs to the league.
     * More on: https://www.football-data.org/documentation/quickstart
     * 
     * @param string $leagueCode
     * @return mixed 
     */
	public function getLeagueInformation(string $leagueCode)
    {
        $endpoint = Str::replaceFirst('{{leagueId}}', $leagueCode, self::$TEAMS_ENDPOINT);

		try {
			$response = $this->http->request(
				'GET',
				$endpoint,
				[
					'headers'   => [
						'X-Auth-Token' => $this->accessToken,
					]
				]
            );
            
            return \json_decode( $response->getBody() );


        } catch ( ClientException $e ) {

            if ($e->getCode() == 429) {
                sleep(self::SECONDS_BETWEEN_REQUEST);
                return $this->getLeagueInformation($leagueCode);

            } elseif ($e->getCode() == 404 || $e->getCode() == 400) {
                throw new NotFoundException();
            }

            throw new ServiceUnavailable($e->getMessage(), $e->getCode()) ;
            
		} catch ( Exception $e ) {

            throw new ServiceUnavailable($e->getMessage(), $e->getCode()) ;

        }

    }
    

    /**
     * Gets teams information by id and players and other straff that belongs to the team.
     * More on: https://www.football-data.org/documentation/quickstart
     *
     * @param string $teamId
     * @return mixed
     */
    public function getTeamInformation(string $teamId)
    {
        $endpoint = Str::replaceFirst('{{teamId}}', $teamId, self::$PLAYERS_ENDPOINT);
		try {

			$response = $this->http->request(
				'GET',
				$endpoint,
				[
					'headers' 	  => [
						'X-Auth-Token' => $this->accessToken,
					]
				]
            );
            
            return \json_decode( $response->getBody() );
		
        } catch ( ClientException $e ) {

            if ($e->getCode() == 429) {
                sleep(self::SECONDS_BETWEEN_REQUEST);
                return $this->getTeamInformation($teamId);

            }  elseif ($e->getCode() == 404) {
                throw new NotFoundException();
            }

            throw new ServiceUnavailable($e->getMessage(), $e->getCode()) ;
            
		} catch ( Exception $e ) {

            throw new ServiceUnavailable($e->getMessage(), $e->getCode()) ;
            
        }
	}

}