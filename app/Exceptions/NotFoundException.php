<?php

namespace App\Exceptions;

class NotFoundException extends \Exception 
{

	public function __construct ($message = 'The resource you are looking for does not exist!') {

		parent::__construct ($message, 404);
	}
}