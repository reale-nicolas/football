<?php

namespace App\Exceptions;

use Exception;

class ConflictException extends Exception
{
 
    public function __construct($message = 'The record already exist.')
    {
        parent::__construct ($message, 409);
    }
}
