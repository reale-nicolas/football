<?php

namespace App\Exceptions;

class ServiceUnavailable extends \Exception 
{

	public function __construct ($message, $code ) {

		parent::__construct ($message, $code );
	}
}