
## Author
    Name: Reale, Nicolas Joaquín
    Email: reale.nicolas@gmail.com
    LinkedIn: https://www.linkedin.com/in/nicolas-reale-a52b5b14/


### Steps to make it work

- Create an empty database on the local environment.
- Copy .env.example file to .env
- Add the database configuration parameters to .env file.
- Add the token to connect with api.football-data.org.
- Run 'composer install'.
- Run 'php artisan migrate'.
- That's it, start making request to '/import-league/{leagueCode}' and '/total-players/{leagueCode}' endpoints.

### Note
    There is .sql file into /storage/database/dump.sql just in case, wicj contains the database structure.
